<?php

/**
 * Combine an minify js or css together and save it in a path
 */


namespace Voodoo\Component\Minify;

Class File{

    CONST CSS = "css";
    CONST JS = "js";
    
    private $fileType = "";
    private $content = "";
    private $files = [];
    private $latestFileMTime = 0;
    
    /**
     * Add files
     * @param array $files
     * @return \Lib\Component\Minify\File
     */
    public function addFiles(Array $files)
    {
        $this->files = $files;
        $this->fileType = pathinfo($files[0], PATHINFO_EXTENSION);     
        return $this;
    }

    
    /**
     * To create the file
     * @param string $filename
     * @param bool $minify
     * @return \Lib\Component\Minify\Minify
     */
    public function minify($filename, $minify = true)
    {
        $this->combineFiles();
        
        $content = $this->content;
        if ($minify) {
            if ($this->fileType == self::JS) {
                $content = JSMin::minify($content);
            } else if ($this->fileType == self::CSS) {
                $content = CSSMin::minify($content);
            }            
        }
        if ($content) {
            file_put_contents($filename, $content);
        }
        return $this;        
    }
    
    private function combineFiles()
    {
        if (count($this->files) && ! $this->content) {
            $this->content = "";
            foreach ($this->files as $file) {
                $this->content .= "\n";
                $this->content .= @file_get_contents($file);

                // Mark the last modified files
                $lastMod = @filemtime($file);
                if ($lastMod > $this->latestFileMTime) {
                   $this->latestFileMTime = $lastMod;
                }       
            } 
        }
    }
}

