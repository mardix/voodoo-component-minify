<?php

/**
 * File
 *
 * Allow to MINIFY and COMPRESS with GZIP a JS or CSS file, and then cache it on the server with E-TAG for faster download
 *
 * It also can combine multiple files and save them into one file to be served
 *
 * What about the original files?
 * The original files will not change. All the comments and space and everything will still be there
 *
 * What if a file has changed?
 * It will recreate the cached version and send it to the browser again
 *
 * What type of files are accepted
 * Only JS and CSS can compressed and cached
 *
 *
 * @version 1.0
 * @since: Aug 11 2009
 * @license: MIT
 * @author: Mardix
 * 
 * ** updated May 22 2011
 */

/**
 * TO MINIFY we'll use JSMin and CSSMin Library.
 *
 *
 */

namespace Lib\Component\Minify;

Class File{

    /**
     * Set the cache dir
     * @param type $cacheDir 
     */
    public function __construct($cacheDir=""){
        $this->minify = true;
        
        if($cacheDir)
            $this->setCacheDir ($cacheDir);
    }
    
    
    /**
     * Set the directory
     * @param <type> $path
     * @return <type>
     */
    public function setCacheDir($path){
     $this->cacheDir=$path;
     return $this;
    }

    /**
     * Set the type of files
     * @param <type> $type js | css
     * @return <type>
     */
    public function setType($type){
     $this->type=$type;
     return $this;
    }

    /**
     * To save the files
     * @param Array $rFiles
     * @return <type>
     */
    public function add($rFiles){
    /* files in array*/
     $this->files=$rFiles;
     return $this;
    }

    /**
     * To allow minification
     * @return <type>
     */
    public function doMinify(){
        
     $this->minify = true;
     
     return $this;
    }

    /**
     * To not ninify
     * @return File 
     */
    public function keepRaw(){
        
        $this->minify = false;
        
        return $this;
    }

//------------------------------------------------------------------------------
    //Let's get to work
    public function exec(){
        
            // Prepare some header info
            if($this->type=="js"){
             $FILE_PREFIX="js";
             $HEADER_TYPE="text/javascript";
            }

            else if($this->type=="css"){
             $FILE_PREFIX="css";
             $HEADER_TYPE="text/css";
            }


       // Disable zlib compression, if present, for duration of this script.
       // So we don't double gzip
       ini_set("zlib.output_compression", "Off");

       // Set the content type header
       header("Content-Type: $HEADER_TYPE; charset=UTF-8");

       // Set the cache control header
       // http 1.1 browsers MUST revalidate -- always
       header("Cache-Control: must-revalidate");

       // Hold last mod
       $newestFile = 0;

       // This is generated for the Hash
       $longFilename = ''; 

       // Valid JS | CSS files
       $fileNames = Array();

       // Go thru each file and get the last update
       foreach($this->files as $file){
           
               // make sure file is js or css
               if (preg_match('/\.js$|\.css$/i',$file)){

                     // Save each valid JS & CSS files
                     $fileNames[] = $file;

                     // Create the long filename fo
                     $longFilename .= $file;

                     // Get file last modified time
                     $lastMod = @filemtime($file);

                     // Is this the newest file, mark it
                     if ($lastMod > $newestFile)
                        $newestFile = $lastMod;

               }
       }


    /////////////////////////////////////////////////////////////////////////////
    // Begin *BROWSER* Cache Control

       // Here we check to see if the browser is doing a cache check
       // First we'll do an etag check which is to see if we've already stored
       // the hash of the filename . '-' . $newestFile.  If we find it
       // nothing has changed so let the browser know and then die.  If we
       // don't find it (or it's a mismatch) something has changed so force
       // the browser to ignore the cache.

       // This generates a key from the collective file names
       $fileHash = md5($longFilename);      
       
       // This appends the newest file date to the key.
       $hash = $fileHash .'-'.$newestFile;  

       // Get all the headers the browser sent us.
       $headers = getallheaders();       
       
       // Look for a hash match
       if (preg_match("/$hash/", $headers['If-None-Match']))  {   
          // Our hash+filetime was matched with the browser etag value so nothing
          // has changed.  Just send the last modified date and a 304 (nothing changed)
          // header and exit.
          header('Last-Modified: '.gmdate('D, d M Y H:i:s', $newestFile).' GMT', true, 304);
          
          die();
       }

       // We're still alive so save the hash+latest modified time in the e-tag.
       header("ETag: \"{$hash}\"");

       // For an additional layer of protection we'll see if the browser
       // sent us a last-modified date and compare that with $newestFile
       // If there's no change we'll send a cache control header and die.

       if (isset($headers['If-Modified-Since'])) {

          if ($newestFile <= strtotime($headers['If-Modified-Since'])) {
             // No change so send a 304 header and terminate
              header('Last-Modified: '.gmdate('D, d M Y H:i:s', $newestFile).' GMT', true, 304);
              
              die();
           }
       }

       // Set the last modified date as the date of the NEWEST file in the list.
       header('Last-Modified: '.gmdate('D, d M Y H:i:s', $newestFile).' GMT');

    // End *BROWSER* Cache Control
    /////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////
    // Begin File System Cache Control

       // Attempt to open a cache file for this set.  (This is the server file-system
       // cache, not the browser cache.  From here on out we're done with the browser cache

       $fp = @fopen($this->cacheDir."/$fileHash.txt","r");

       if ($fp) {
          // A cache file exists but if contents have changed delete the file pointer
          // so we re-process the files like there was no cache
          if ($newestFile>@filemtime($this->cacheDir."/$fileHash.txt")) {
              
             fclose($fp);
             
             $fp=false;
             
          }
       }

       if (!$fp) {
           
          // CREATE THE CONTENT
          // No file pointer exists so we create the cache files for this set.
          // for each filename in $fileNames, put the contents into $buffer
          // with two blank lines between each file.
          $buffer='';

          foreach($fileNames as $thisFile){
              
            $buffer .= @file_get_contents($thisFile) . "\n\n";
            
          }
          
          // MINIFY THE CONTENT BEFORE IT GOES OUT
          $buffer = $this->_minify($buffer);


          // We've created our concatenated file so first we'll save it as
          // plain text for non gzip enabled browsers.
          $fp = @fopen($this->cacheDir."/$fileHash.txt","w");
          @fwrite($fp,$buffer);
          @fclose($fp);

          // Now we'll compress the file (maximum compression) and save
          // the compressed version.
          $fp = @fopen($this->cacheDir."/$fileHash.gz","w");
          $buffer = gzencode($buffer, 9, FORCE_GZIP);
          @fwrite($fp,$buffer);
          @fclose($fp);
       }

    // End File System Cache Control
    /////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////
    // Begin Output

       // Browser can handle gzip data so send it the gzip version
       if (strstr($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) {
           
          header ("Content-Encoding: gzip");

          header ("Content-Length: " . filesize($this->cacheDir."/$fileHash.gz"));

          readfile($this->cacheDir."/$fileHash.gz");
          
       } 
       
       // Browser can't handle gzip so send it plain text version.
       else {
          
          header ("Content-Length: " . filesize($this->cacheDir."/$fileHash.txt"));

          readfile($this->cacheDir."/$fileHash.txt");
       }
    }


//------------------------------------------------------------------------------

    /**
     * To minify
     * @param <type> $content
     * @return <type>
     */
    private function _minify($content){

      if($this->minify==true){

          if($this->type=="js")
            return JSMin::minify($content);

          else if($this->type=="css")
            return CSSMin::minify($content);

          else
            return $content;
      }

      else
        return $content;
    }

//------------------------------------------------------------------------------

}

/**
 * On the server, php is being ran as fcgi, this function doesn't work, so we'll create it
 */
if (!function_exists('getallheaders')){
    function getallheaders()
    {
       foreach ($_SERVER as $name => $value)
       {
           if (substr($name, 0, 5) == 'HTTP_')
           {
               $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
           }
       }
       return $headers;
    }
}
