
Component to combine and minify JS or CSS files


ie:

(Voodoo\Component\Minify\File)
    ->addFiles([
        "/file.js",
        "/file2.js"
    ])
    ->minify("path/filename.min.js");